package com.advento.firstms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.advento.firstms.service.PatternProgramService;

public class PatternProgramController {

	
	@Autowired
	@Qualifier(value = "patternServiceImpl")
	private PatternProgramService patternService;

	@GetMapping(value = "/pattern")
	public ResponseEntity<String> rightAnglePattern(
			@RequestParam(value = "source", required = false) String source,
			@RequestParam(value = "length") int length) {
		String pattern = patternService.rightAnglePattern(source, length);
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(pattern, HttpStatus.OK);
		return responseEntity;
	}

	
	
}
