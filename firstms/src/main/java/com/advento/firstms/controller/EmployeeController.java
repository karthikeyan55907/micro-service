package com.advento.firstms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.advento.firstms.dto.EmployeeDTO;
import com.advento.firstms.service.EmployeeService;





@RestController
public class EmployeeController {

	
	@Autowired
	@Qualifier (value="employeeServiceImpl")
	private EmployeeService employeeService; 
	
	
	@GetMapping(value ="/Employees")
	public ResponseEntity<EmployeeDTO[]> getEmployees(){
		EmployeeDTO[] empDTOs = employeeService.getEmployees();
		ResponseEntity<EmployeeDTO[]> responseEntity = new ResponseEntity<EmployeeDTO[]>(empDTOs, HttpStatus.OK);
		return responseEntity;
	}
}
