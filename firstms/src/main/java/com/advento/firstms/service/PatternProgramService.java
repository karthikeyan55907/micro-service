package com.advento.firstms.service;

public interface PatternProgramService {

	public String rightAnglePattern(String source, int length);
}
