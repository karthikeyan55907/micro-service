package com.advento.firstms.service.impl;

import org.springframework.stereotype.Service;

import com.advento.firstms.service.PatternProgramService;


@Service(value = "patternServiceImpl")

public class PatternProgramServiceImpl  implements PatternProgramService {

	@Override
	public String rightAnglePattern(String source, int length) {
		String pattern = "";
		if (length > 0) {
			for (int i = 1; i <=length; i++) {
				for (int j = 1; j<=i; j++) {
					pattern = pattern + "*";
				}
				if (source != null && source.equalsIgnoreCase("postman")) {
					pattern = pattern + "\n";
				} else {
					pattern = pattern + "<br />";
				}
			}
		}
		return pattern;
	}

}
