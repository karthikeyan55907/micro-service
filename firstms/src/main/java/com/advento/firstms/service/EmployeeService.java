package com.advento.firstms.service;

import com.advento.firstms.dto.EmployeeDTO;

public interface EmployeeService {
	
	public EmployeeDTO[] getEmployees();

}
