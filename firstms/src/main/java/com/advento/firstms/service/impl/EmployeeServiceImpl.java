package com.advento.firstms.service.impl;

import org.springframework.stereotype.Service;

import com.advento.firstms.dto.EmployeeDTO;
import com.advento.firstms.service.EmployeeService;

@Service (value="employeeServiceImpl")
public class EmployeeServiceImpl implements EmployeeService {

	@Override
	public EmployeeDTO[] getEmployees() {

		 EmployeeDTO empDTO1 = createEmpObj(1, "karthik", 22);
		 EmployeeDTO empDTO2 = createEmpObj(2, "gopi", 25);
		 EmployeeDTO empDTO3 = createEmpObj(3, "arun", 28);
		 EmployeeDTO empDTO4 = createEmpObj(4, "subash", 36);
		 EmployeeDTO empDTO5 = createEmpObj(5, "guru", 26);
		
		EmployeeDTO[] empDTOs=new EmployeeDTO[5];
				empDTOs[0]=empDTO1;
				empDTOs[1]=empDTO2;
				empDTOs[2]=empDTO3;
				empDTOs[3]=empDTO4;
				empDTOs[4]=empDTO5;
				return empDTOs;
				
		
	}
	private EmployeeDTO createEmpObj(int id, String name, int age) {
		EmployeeDTO empDTO = new EmployeeDTO();
		 empDTO.setId(id);
		 empDTO.setName(name);
		 empDTO.setAge(age);
		
		return empDTO;
	}

	
	
}
