package com.advento.numberpattern;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NumberpatternApplication {

	public static void main(String[] args) {
		SpringApplication.run(NumberpatternApplication.class, args);
	}

}
