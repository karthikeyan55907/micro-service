package com.advento.numberpattern.service;

public interface NumberPatternService {
	
	public String numberPattern1();
	public String numberPattern2();
	public String numberPattern3();

}
