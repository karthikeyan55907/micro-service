package com.advento.numberpattern.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.advento.numberpattern.service.NumberPatternService;

@RestController
public class NumberPatternController {
	
	@Autowired
	@Qualifier(value = "numberPatternServiceImpl")
	private NumberPatternService numberPatternService;
	
	@GetMapping(value = "/patterna")
	public  ResponseEntity<String> numberPattern1() {
		String pattern = numberPatternService.numberPattern1();
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(pattern, HttpStatus.OK);
		return responseEntity;
	}
	
	@GetMapping(value = "/patternb")
	public  ResponseEntity<String> numberPattern2() {
		String pattern = numberPatternService.numberPattern2();
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(pattern, HttpStatus.OK);
		return responseEntity;
	}
	
	@GetMapping(value = "/patternc")
	public  ResponseEntity<String> numberPattern3() {
		String pattern = numberPatternService.numberPattern3();
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(pattern, HttpStatus.OK);
		return responseEntity;
	}

}
