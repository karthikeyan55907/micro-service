package com.advento.samplems;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SamplemsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SamplemsApplication.class, args);
	}

}
