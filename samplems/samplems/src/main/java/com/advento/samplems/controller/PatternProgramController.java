package com.advento.samplems.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.advento.samplems.service.PatternProgramService;
@RestController
public class PatternProgramController {

	@Autowired
	@Qualifier(value = "patternServiceImpl")
	private PatternProgramService patternProgramService;

	@GetMapping(value = "/pattern")
	public ResponseEntity<String> rightAnglePattern() {

		String pattern = patternProgramService.rightAnglePattern();
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(pattern, HttpStatus.OK);
		return responseEntity;
	}
	
	@GetMapping(value = "/pat")
	public ResponseEntity<String> rightAnglePatternDown() {

		String pattern1 = patternProgramService.rightAnglePatternDown();
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(pattern1, HttpStatus.OK);
		return responseEntity;
	}
	@GetMapping(value = "/patterndimond")
	public ResponseEntity<String> rightAnglePatternDiamond() {

		String pattern1 = patternProgramService.rightAnglePatternDiamond();
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(pattern1, HttpStatus.OK);
		return responseEntity;
	}
	
	@GetMapping(value = "/patterncenter")
	public ResponseEntity<String> centerAnglePattern() {

		String pattern2 = patternProgramService.centerAnglePattern();
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(pattern2, HttpStatus.OK);
		return responseEntity;
	}
	
	@GetMapping(value = "/patterncenterdown")
	public ResponseEntity<String> centerAnglePatternDown() {

		String pattern3 = patternProgramService.centerAnglePatternDown();
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(pattern3, HttpStatus.OK);
		return responseEntity;
	}
	
	@GetMapping(value = "/patterncenterdiamond")
	public ResponseEntity<String> centerAnglePatternDiamond() {

		String pattern4 = patternProgramService.centerAnglePatternDiamond();
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(pattern4, HttpStatus.OK);
		return responseEntity;
	}
	
}
