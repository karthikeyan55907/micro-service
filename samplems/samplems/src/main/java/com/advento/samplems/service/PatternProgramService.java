package com.advento.samplems.service;

public interface PatternProgramService {

	//public String rightAnglePattern(String source, int length);

	public String rightAnglePattern();
	
	public String rightAnglePatternDown();
	public String rightAnglePatternDiamond();
	public String centerAnglePattern();
	public String centerAnglePatternDown();
	public String centerAnglePatternDiamond();
}

