package com.advento.samplems.service.impl;

import org.springframework.stereotype.Service;

import com.advento.samplems.service.PatternProgramService;



@Service(value = "patternServiceImpl")

public class PatternProgramServiceImpl implements PatternProgramService {
	
    @Override
	public String rightAnglePattern() {
		String pattern = "";
			for (int i = 1; i <=5; i++) {
				for (int j = 1; j<=i; j++) {
					pattern = pattern + "* ";
				}
					pattern = pattern + "\n";
			}
		return pattern;
	}
    @Override
    public String rightAnglePatternDown() {
		String pattern = "";
		 for ( int m = 5; m >= 1; m--) {
			 for (int n = 1; n <= m; n++) {
					pattern = pattern + "* ";
				}
					pattern = pattern + "\n";
			}
		return pattern;
	}

    @Override
    public String rightAnglePatternDiamond() {
    			String pattern = "";
    
    for (int i = 1; i <=5; i++) {
		for (int j = 1; j<=i; j++) {
			pattern = pattern + "* ";
		}
			pattern = pattern + "\n";
	}
		 for ( int m = 5; m >= 1; m--) {
			 for (int n = 1; n <= m; n++) {
					pattern = pattern + "* ";
				}
					pattern = pattern + "\n";
			}
    
			return pattern;
			}
		
    
    
    
    
    
    
    @Override
   	public String centerAnglePatternDown() {
  					
    	  String a="";
          for (int i = 1; i <= 5; i++) {
              for (int j = 0; j <= i; j++) {
                  a+=(" ");
              }
              for (int k = 0; k < 6 - i; k++) {
                  a+=("* ");
              }
              a+=("\n");
          }
          return a;
    }
    
    @Override
   	public String centerAnglePattern() {
  					
    	  String b="";
          int i, j, k;
          for (i = 0; i < 6; i++) {
              for (j = 6 - i; j > 1; j--) {
                  b +=(" ");
              }
              for (k = 0; k <= i; k++) {
                  b +=("* ");
              }
              b +=("\n");
          }
          return b;
    }
    
    @Override
   	public String centerAnglePatternDiamond() {
  					
    	  String b="";
    	  
          for (int i = 0; i < 5; i++) {
              for (int j = 6 - i; j > 1; j--) {
                  b +=(" ");
              }
              for (int k = 0; k <= i; k++) {
                  b +=("* ");
              }
              b +=("\n");
          }
         
          for (int i = 1; i <= 5; i++) {
              for (int j = 0; j <= i-1; j++) {
                  b+=(" ");
              }
              for (int k = 0; k < 6 - i; k++) {
                  b+=("* ");
              }
              b+=("\n");
          }
        
          return b;
    }
    
    
    
}
    
    
	

