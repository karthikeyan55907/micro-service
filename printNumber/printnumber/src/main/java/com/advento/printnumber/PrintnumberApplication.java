package com.advento.printnumber;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrintnumberApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrintnumberApplication.class, args);
	}

}
