package com.advento.printnumber.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.advento.printnumber.service.PrintNumberService;
@RestController
public class PrintNumberController {

	@Autowired
	@Qualifier(value = "PrintNumberServiceImpl")
	private PrintNumberService numberService;
	
	@GetMapping(value = "/patterna")
	public  ResponseEntity<String> divideNumber() {
		String pattern = numberService.divideNumber();
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(pattern, HttpStatus.OK);
		return responseEntity;
	}
	
	@GetMapping(value = "/patternb")
	public  ResponseEntity<String> evenNumber() {
		String pattern1 = numberService.evenNumber();
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(pattern1, HttpStatus.OK);
		return responseEntity;
	}
	@GetMapping(value = "/patternc")
	public  ResponseEntity<String> oddNumber() {
		String pattern2 = numberService.oddNumber();
		ResponseEntity<String> responseEntity = new ResponseEntity<String>(pattern2, HttpStatus.OK);
		return responseEntity;
	}
	
}
