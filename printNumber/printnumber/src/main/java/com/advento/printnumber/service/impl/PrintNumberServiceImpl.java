package com.advento.printnumber.service.impl;

import org.springframework.stereotype.Service;

import com.advento.printnumber.service.PrintNumberService;
@Service(value="PrintNumberServiceImpl")
public class PrintNumberServiceImpl implements PrintNumberService{
	
	
	public  String divideNumber() {
        String a="";
        for (int i = 1; i <= 100; i++) {
            if (i % 11 == 0) {
                a+=(i + " ");
                }
            }
        return a;
    }
    

    @Override
    public String evenNumber() {
        String b="";
        for (int i = 1; i <= 100; i++) {
            if (i % 2 == 0) {
                b+=(i + " ");
            }
        }
        return b;
    }
    
    public String oddNumber() {
        String c="";
        for (int i = 1; i <= 100; i++) {
            if (i % 2 != 0) {
                c+=(i + " ");
            }
        }
        return c;
    }

}
